--
-- removing and recreating mac_address device index
--
DROP INDEX IF EXISTS ux_devices_mac_address;
CREATE UNIQUE INDEX ux_devices_mac_address ON
devices (gateway_id, mac_address);
