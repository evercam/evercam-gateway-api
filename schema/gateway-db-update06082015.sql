-- Update 6th August 2015. Adding extra fields from Evercam Discovery

--
-- devices
--
ALTER TABLE devices 
ADD COLUMN friendly_name text,
ADD COLUMN default_password text,
ADD COLUMN default_username text,
ADD COLUMN http_jpg_path text,
ADD COLUMN rtsp_h264_path text,
ADD COLUMN vendor_id text,
ADD COLUMN model_id text,
DROP COLUMN IF EXISTS thumbnail,
ADD COLUMN model_thumbnail_url text,
ADD COLUMN vendor_thumbnail_url text;


