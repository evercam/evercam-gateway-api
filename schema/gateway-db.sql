﻿--
-- pending gateways
--
CREATE SEQUENCE sq_pending_gateways;

CREATE TABLE pending_gateways
(
   temporary_id int NOT NULL DEFAULT nextval('sq_pending_gateways'),
   CONSTRAINT pk_pending_gateways PRIMARY KEY (temporary_id),
   mac_address macaddr NOT NULL,
   originating_ip_address inet NOT NULL,
   created_at timestamptz DEFAULT CURRENT_TIMESTAMP,
   updated_at timestamptz,
   polled_at timestamptz
);

CREATE UNIQUE INDEX ux_pending_gateways_mac_address
ON pending_gateways (mac_address);

---
-- gateways
-- 
CREATE SEQUENCE sq_gateways;

CREATE TABLE gateways
(
   id int NOT NULL DEFAULT nextval('sq_gateways'),
   CONSTRAINT pk_gateways PRIMARY KEY (id),
   user_id text NOT NULL,
   name text,
   mac_address macaddr NOT NULL,
   registration_status text,
   networks json,
   vpn_local_hostname text,
   vpn_hostname text,
   vpn_port int,
   vpn_hub text,
   vpn_username text,
   vpn_mac_address macaddr,
   vpn_ip_address inet,
   created_at timestamptz DEFAULT CURRENT_TIMESTAMP,
   registered_at timestamptz,
   last_contact_at timestamptz
);

CREATE UNIQUE INDEX ux_gateways_mac_address
ON gateways (mac_address);

--
-- devices
--
CREATE SEQUENCE sq_devices;

CREATE TABLE devices
(  
  id int NOT NULL DEFAULT nextval('sq_devices'),
  CONSTRAINT pk_devices PRIMARY KEY (id),
  gateway_id int NOT NULL,
  mac_address macaddr NOT NULL,
  ip_address inet,
  name text,
  thumbnail text,
  notes text,
  ports json,
  created_at timestamptz DEFAULT CURRENT_TIMESTAMP
);

CREATE UNIQUE INDEX ux_devices_mac_address ON
devices (mac_address);

CREATE INDEX ix_devices_gateway_id ON
devices (gateway_id);

--
-- tokens
--
CREATE SEQUENCE sq_tokens;

CREATE TABLE tokens
(
   id int NOT NULL DEFAULT nextval('sq_tokens'),
   CONSTRAINT pk_tokens PRIMARY KEY (id),
   gateway_id int NOT NULL,
   token text NOT NULL,
   issued_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
   revoked boolean NOT NULL DEFAULT false,
   expires_in int NOT NULL DEFAULT 2
);

CREATE INDEX ix_tokens_gateway_id
ON tokens (gateway_id);

CREATE UNIQUE INDEX ux_tokens_token
ON tokens (token);

--
-- secrets
--
CREATE SEQUENCE sq_secrets;

CREATE TABLE secrets
(
   id int NOT NULL DEFAULT nextval('sq_secrets'),
   CONSTRAINT pk_secrets PRIMARY KEY(id),
   mac_address macaddr NOT NULL,
   secret text NOT NULL,
   m2m_secret text NOT NULL,
   issued boolean NOT NULL DEFAULT false,
   issue_date timestamptz,
   created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE UNIQUE INDEX ux_secrets_mac_address
ON secrets (mac_address);

--
-- foreign keys
--

-- gateways -> devices
ALTER TABLE devices
ADD CONSTRAINT fk_devices_gateway_id
FOREIGN KEY (gateway_id) REFERENCES gateways (id)
ON DELETE CASCADE;

-- gateways -> tokens
ALTER TABLE tokens
ADD CONSTRAINT fk_tokens_gateway_id
FOREIGN KEY (gateway_id) REFERENCES gateways (id)
ON DELETE CASCADE;
