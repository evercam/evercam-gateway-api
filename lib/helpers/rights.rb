module EvercamGateway

  class Rights

    attr_accessor :evercam, :ids

    def initialize
      @ids = []
      @evercam = nil
    end

    def includes?(id)
      @ids.include?(id.to_i)
    end
    
  end

end
