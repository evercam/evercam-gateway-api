module EvercamGateway

  class Validation

    def self.valid_mac_address?(str)
      !!(str =~ /\A([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})\z/) 
    end

    def self.valid_json?(json)
      begin
        JSON.parse(json)
        return true
      rescue Exception => e
        return false
      end
    end
    
  end

end
