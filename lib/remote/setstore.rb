require 'redis'
require 'connection_pool'

module EvercamGateway

  class Setstore

    @@pool = ConnectionPool.new(size: 3, timeout: 5) do
      ::Redis.new(url: ENV["REDIS_URL"])
    end

    def self.pool
      @@pool
    end
    
  end

end
