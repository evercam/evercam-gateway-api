module EvercamGateway
 
  class Evercam

    attr_reader :id, :token

    def initialize(id, token)
      @id = id
      @token = token
    end

    def self.authenticate(token)
      # Make request to auth.evercam.io for basic user info
      evercam = open("https://auth.evercam.io/oauth2/tokeninfo?access_token=#{token}").read
      evercam = JSON.parse(evercam, {symbolize_names: true})
      
      if evercam[:userid]
        self.new evercam[:userid], evercam[:access_token]
      else
        nil
      end
    end
    
  end

end
