require "socket"
require "thread"
require "openssl"
require "netstring"
require "thread"
require "thwait"

module EvercamGateway

  class VPNService
    
    def self.get_gateway_configuration_data(id)
      request = {:token => ENV["VPN_SERVICE_TOKEN"], :gateway_id => id.to_i}
      json_request = request.to_json
      net_string_request = Netstring.dump(json_request)
      send_data(net_string_request)
    end

    def self.send_data(request)
      host = ENV["VPN_SERVICE_HOST"]
      port = ENV["VPN_SERVICE_PORT"]

      socket = TCPSocket.new(host, port)
      ssl = OpenSSL::SSL::SSLSocket.new(socket)
      ssl.sync_close = true
      ssl.connect

      ssl.puts request

      data_netstring = ""
      data = ""

      threads = []

      threads << Thread.new {
        begin
          while lineIn = ssl.gets
            lineIn = lineIn.chomp
            data_netstring = data_netstring+lineIn
            begin
              data = Netstring.load(data_netstring)
            rescue
              # Not a valid netstring keep reading
            end
          end
        rescue
            # A connection error of some kind
        end
      }

      ThreadsWait.all_waits(*threads)
      data 
    end
    
  end
  
end
