module EvercamGateway

  class Ports
    
    PORT_RANGE_START=1024
    PORT_RANGE_END=32768
    # Ideally Gateway would not have any ports reserved in this range
    # but it seems likely to come up.
    PORT_EXCLUDE=[8080, 2222]
    
    # Get random port in range and ensure it is not in exclusion list
    def self.get_random_port
      port = nil
      loop do
        port = rand(PORT_RANGE_START..PORT_RANGE_END)
        break if !PORT_EXCLUDE.include?(port)
      end
      port
    end

    # find an unused port for a specific redis set and add
    def self.reserve_port(key)
      port = nil
      Setstore.pool.with {|conn|
        loop do
          port = get_random_port
          result = conn.sadd(key,port)
          break if result
        end
      }
      port
    end

    def self.release_port(key, port)
      result = nil
      Setstore.pool.with {|conn|
        result = conn.srem(key, port)
      }
      result
    end
    
    # Used fixed algorithm to get next port. This is the port
    # that is "local" to the Gateway. They need only be unique to
    # the specific Gateway
    def self.get_local_port(gateway_id)
      reserve_port("gateway:#{gateway_id}:ports")
    end

    # Dole out a port within accepted range. Port
    # availability is per VPN Server Hostname, i.e. we need
    # to know which VPN Server the Gateway connects to. These
    # are the ports that get publicly bound on an internet
    # accessible IP/Hostname.
    def self.get_public_port(gateway_id)
      gateway = Gateway[gateway_id]
      reserve_port("vpn_server:#{gateway.vpn_hostname}:ports")
    end

    def self.release_local_port(port_id, gateway_id)
      release_port("gateway:#{gateway_id}:ports", port_id)
    end

    def self.release_public_port(port_id, gateway_id)
      gateway = Gateway[gateway_id]
      release_port("vpn_server:#{gateway.vpn_hostname}:ports", port_id)
    end
    
  end

end
