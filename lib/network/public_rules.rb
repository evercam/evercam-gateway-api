module EvercamGateway

  class PublicRules

    def self.add(gateway_id, public_port_id, local_port_id)
      gateway = Gateway[gateway_id]
      key = "vpn_server:#{gateway.vpn_hostname}:public_forwards"
      
      result = nil
      Setstore.pool.with {|conn|
        result = conn.sadd(key, "#{public_port_id}:#{local_port_id}:#{gateway.vpn_ip_address}")
      }
      result
    end

    def self.remove(gateway_id, public_port_id, local_port_id)
      gateway = Gateway[gateway_id]
      key = "vpn_server:#{gateway.vpn_hostname}:public_forwards"
                                                     
      result = nil
      Setstore.pool.with {|conn|
        result = conn.srem(key, "#{public_port_id}:#{local_port_id}:#{gateway.vpn_ip_address}")
      }
      result
    end
    
  end

end
