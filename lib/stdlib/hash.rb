# monkey patch Hash class to limit hash keys based on an array of keys
module StdlibExtensions
  module Hash
    def only(*args)
      self.reject { |k,v| !args.include?(k) }
    end
  end
end
