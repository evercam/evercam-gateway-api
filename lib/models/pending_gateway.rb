module EvercamGateway

  class PendingGateway < Sequel::Model
    
    def self.safe_new(data, originating_ip)
      data = data.only(:mac_address)
      data[:originating_ip_address] = originating_ip
      self.new data
    end
   
  end

end
