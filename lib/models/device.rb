require 'ipaddress'

module EvercamGateway

  class Device < Sequel::Model

    many_to_one :owner, class: 'EvercamGateway::Gateway'
    VALID_NEW_FIELDS = [:mac_address]
    VALID_UPDATE_FIELDS = [:ports, :ip_address, :friendly_name, :default_password,
                    :default_username, :http_jpg_path, :rtsp_h264_path,
                    :vendor_id, :model_id, :model_thumbnail_url, :vendor_thumbnail_url, :evercam_id]

    def validate
      super
      errors.add(:ip_address, 'bad format') if !IPAddress.valid?(ip_address) || ip_address.empty?
    end

    def initialize(*args)
      args[0] = args[0].only(*(VALID_NEW_FIELDS+VALID_UPDATE_FIELDS))
      super
    end

    # override update method to ensure only valid keys are allowed
    def update(data, override_ports=false)
      # Handle any existing forwarded ports intelligently
      if !forwarded_ports.empty? && !override_ports then
        data[:ports] = Device.merge_ports(data[:ports], forwarded_ports)
      end

      super(data.only(*VALID_UPDATE_FIELDS))
    end

    # Checks if any ports on the selected device are forwarded
    def forwarded_ports
      ports = []
      self.ports.each { |port|
        # if its got a gateway port then it's forwarded
        if port["local_port_id"] then
          ports.push(port)
        end
      }
      ports
    end

    # Compares two arrays of hashes of port data. Retains any existing
    # ports that are not in new array. Also keeps any existing fields
    # in matching port hashes that are not in new array. Effectively
    # this ensures that if a port is forwarded it will never be dropped
    # or have its forwarding data dropped. But that in the case that its
    # forwarding data is updated this will be reflected.
    def self.merge_ports(new_ports, existing_ports)
      existing_ports.each { |existing_port|
        # detect port in new that is present in existing
        new_ports = [] if !new_ports
        duplicate_port = new_ports.detect {|_port| _port[:port_id].to_i == existing_port["port_id"].to_i }
        if (duplicate_port)
          # create merge
          merged_port = existing_port.merge(duplicate_port)
          # remove duplicate from new
          new_ports.delete_if {|_port| _port[:port_id].to_i == existing_port["port_id"].to_i }
          # add merged instead
          new_ports.push(merged_port)
        else
          new_ports.push(existing_port)
        end
      }
      new_ports
    end
    
  end

end
