module EvercamGateway

  # A secret associated with a specific MAC Address
  class Secret < Sequel::Model

    def before_create
      self.secret = BCrypt::Password.create(secret)
      super
    end

    def self.safe_new(data)
      data = data.only(:secret, :mac_address)
      self.new data
    end

    # This creates a random string, saves it hashed in secrets table and returns it unhashed
    def generate_m2m_secret
      m2m_secret = SecureRandom.urlsafe_base64
      update({:m2m_secret=>BCrypt::Password.create(m2m_secret)})
      save
      m2m_secret
    end
    
    def self.match?(mac_address, provided_secret, match_m2m_secret=false)
      # Get any matching record for the mac
      secret = Secret.where[mac_address: mac_address]
      
      if secret         
        # Load the hashed secret (either the owner's secret or the m2m secret)
        if !match_m2m_secret then
          match_secret = BCrypt::Password.new(secret.secret)
        else
          match_secret = BCrypt::Password.new(secret.m2m_secret)
        end
        
        # Compare it to the supplied secret. Remember match_secret is an object.
        # Comparison method   is overloaded
        match_secret == provided_secret
      else
        false
      end
    end
    
  end

end
