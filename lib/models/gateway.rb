module EvercamGateway

  class Gateway < Sequel::Model

    one_to_many :devices
    one_to_many :tokens
    
    def initialize(evercam_id, name, pending_gateway)
      values = {:user_id => evercam_id, :name => name, :mac_address => pending_gateway.mac_address,
                :registration_status => "approved"}
      super(values)
    end
    
    def registered?
      self.registration_status == "registered"
    end
    
    def self.registered?(mac_address)
      Gateway.where[mac_address: mac_address, registration_status: "registered"]
    end

    def self.approved?(mac_address)
      Gateway.where[mac_address: mac_address, registration_status: "approved"]
    end

  
    
  end

end
