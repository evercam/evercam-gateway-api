module EvercamGateway

  # Token issued to users of this API
  class Token < Sequel::Model

    many_to_one :owner, class: 'EvercamGateway::Gateway'
    
    def before_create
      self.token = SecureRandom.uuid;
      super
    end

    def valid_token?
      !self.revoked && !valid_until.past?
    end

    def valid_until
      self.issued_at.advance(:seconds => self.expires_in)
    end

  end

end
