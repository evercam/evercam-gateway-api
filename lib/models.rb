require 'sequel'
require 'securerandom'
require 'bcrypt'

# Connect to DB
DB = Sequel.connect(ENV['DATABASE_URL'])
DB.extension :pg_json
Sequel::Model.plugin :json_serializer
Sequel::Model.plugin :timestamps, update_on_create: true
Sequel::Model.plugin :dataset_associations

require_relative './models/pending_gateway.rb'
require_relative './models/secret.rb'
require_relative './models/token.rb'
require_relative './models/gateway.rb'
require_relative './models/device.rb'
