require 'sinatra'
require 'sinatra/respond_to'
require 'active_support'
require 'active_support/core_ext'
require 'json'
require 'rack-ssl-enforcer'
require "rack/utf8_sanitizer"
require 'rack/cors'
require 'open-uri'
require_relative './remote/evercam.rb'
require_relative './remote/setstore.rb'
require_relative './remote/vpn_service.rb'
require_relative './helpers/validation.rb'
require_relative './helpers/rights.rb'
require_relative './network/ports.rb'
require_relative './network/public_rules.rb'
require_relative './stdlib/hash.rb'

module EvercamGateway

  class V1 < Sinatra::Base

    Hash.send :include, StdlibExtensions::Hash
    register Sinatra::RespondTo
    use Rack::UTF8Sanitizer     
  
    # enforce https in production
    configure :production do

      use Rack::SslEnforcer
    end

    # defines the standard content response for sinatra/respond_to
    set :default_content, 'json' 
    
    # Returns a list of gateway ids for which the API user has access
    # For a Gateway token this will be a single id. For an Evercam token
    # it could be any number
    def get_rights
      rights = Rights.new
      
      if evercam_token
        # Authenticate with Evercam to obtain Evercam User ID
        evercam = Evercam.authenticate(evercam_token)
        if evercam then
          # Add all the ids for which the user has rights
          gateways = Gateway.where(user_id: evercam.id)
          gateways.each { |gateway| rights.ids.push(gateway.id) }

          # Also add the Evercam object itself at the end
          rights.evercam = evercam
        end
      else
        token = Token::first(token: auth_token)
        if token && token.valid_token? then
          # Set Last Contact
          Gateway[token.gateway_id].update(:last_contact_at => Time.now)
          
          # Create rights array
          rights.ids.push(token.gateway_id)
          rights.evercam = nil
        end
      end

      rights
    end
    
    private    
      # ensures the response is formatting according to the desired format
      # at present only supporting json
      def format_response(package)
        respond_to do |wants|
          wants.json { package.to_json }
        end
      end

      # method to automatically get json from post data
      def json_data
        request.body.rewind
        body = request.body.read
        if Validation.valid_json?(body) then
          JSON.parse(body, {symbolize_names: true})
        else
          {}
        end
      end

      def raw_data
        request.body.rewind
        request.body.read
      end
      
      # method to extract token from auth header - note the single whitespace after 'Token'
      def auth_token
        request.env["HTTP_AUTHORIZATION"].to_s.partition("Token ").last
      end

      # method to obtain Evercam token from params or json body
      def evercam_token
        token = json_data[:evercam_token] unless json_data.is_a?(Array)
        if token then
          token
        else
          params[:evercam_token]
        end
      end

      # Error Messages
      def error_operation_failed
        {error: "The requested operation failed."}
      end

      def error_authentication_failed
        {error: "You did not authenticate successfully."}
      end

      def error_not_authenticated
        {error: "You provided no authentication."}
      end

      def error_token_no_longer_valid
        {error: "The token provided is no longer valid. It has either been revoked or has expired."}
      end

      def error_gateway_not_approved
        {error: "This gateway is still pending approval."}
      end

      def error_not_implemented
        {error: "This functionality is not yet implemented"}
      end
  end

end

require_relative './models'
require_relative './v1/gateways_registration.rb'
require_relative './v1/gateways.rb'
require_relative './v1/networks.rb'
require_relative './v1/secrets.rb'
require_relative './v1/devices.rb'
require_relative './v1/rules.rb'
