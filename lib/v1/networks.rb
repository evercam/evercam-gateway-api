module EvercamGateway

  class V1 < Sinatra::Base

    post '/gateways/:id/networks' do |id|
      rights = get_rights
      halt 401, format_response(error_authentication_failed) unless rights.includes?(id)
      
      # TODO: validate json structure
      gateway = Gateway[id]
      gateway.update(networks: json_data)

      format_response(gateway)
    end

  end
  
end
