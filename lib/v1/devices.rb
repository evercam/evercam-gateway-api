module EvercamGateway

  class V1 < Sinatra::Base

    get '/gateways/:id/devices/?:device_id?' do |id, device_id|
      rights = get_rights
      halt 401, format_response(error_authentication_failed) unless rights.includes?(id)

      devices = Device.where(gateway_id: id)
      if device_id then
        devices = devices.where(id: device_id).first
      else
        devices = devices.all
      end
      
      format_response(devices)
    end
    
    post '/gateways/:id/devices' do |id|
      rights = get_rights
      halt 401, format_response(error_authentication_failed) unless rights.includes?(id)
      
      devices = []
    
      json_data.each { | device |

        existing_device = Device.for_update.first(gateway_id: id, mac_address: device[:mac_address])
        
        # Remove any empty services/ports
        device[:ports] = device[:ports].select {|port| !port.empty?  }

        if existing_device then
          existing_device.update(device)
          devices.push(existing_device)
        else
          new_device = Device.new(device)
          new_device[:gateway_id] = id
          begin
            new_device.save
          rescue
            new_device = {:error => $!.to_s}
          end
          devices.push(new_device)
        end
      }

      format_response(devices)
        
    end

    patch '/gateways/:id/devices/:device_id' do |id, device_id|
      rights = get_rights
      halt 401, format_response(error_authentication_failed) unless rights.includes?(id)
      updates = json_data

      existing_device = Device.for_update.first(id: device_id, gateway_id: id)
      halt 404 unless existing_device

      # If ports are being updated then make sure no empty ones are allowed
      if updates.key?(:ports) then
       updates[:ports] = updates[:ports].select { |port| !port.empty?  }
      end 
        
      existing_device.update(updates)
      status 204
    end

  end

end
