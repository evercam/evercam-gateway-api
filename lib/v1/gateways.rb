module EvercamGateway

  class V1 < Sinatra::Base
    
    # Optional parameter ?status=approved | registered
    # This uses Evercam Authorisation for user ?evercam_token=
    get '/gateways/?:id?' do |id|
      rights = get_rights
      evercam = rights.evercam
      status = params[:status]

      if id then
        halt 401, format_response(error_authentication_failed) unless rights.includes?(id)
        gateway = Gateway.where(id: id).first
        gateway = gateway.where(registration_status: status) unless !status
        halt 404 unless gateway
        format_response(gateway)
      else
        halt 401, format_response(error_authentication_failed) unless evercam
        gateways = Gateway.where(user_id: evercam.id)
        gateways = gateways.where(registration_status: status) unless !status
        halt 404 unless gateways
        format_response(gateways.all)
      end
      
    end

  end
  
end
