module EvercamGateway

  class V1 < Sinatra::Base

    get '/gateways/:id/rules' do |id|
      rights = get_rights
      halt 401, format_response(error_authentication_failed) unless rights.includes?(id)
      
      rules = []
      devices = Device.where(gateway_id: id)

      devices.each { |device|

        device.ports.each { |port|
          # If the port definition contains a local_portid then it is
          # supposed to be forwarded by the gateway, otherwise ignore
          if port["local_port_id"] then
            # And why the fuck are we using strings as keys instead of
            # symbols? Because Sequel tells us we must when dealing
            # with postgres json fields: http://sequel.jeremyevans.net/rdoc-plugins/files/lib/sequel/extensions/pg_json_rb.html 
            rule = {:mac_address => device.mac_address,
                    :ip_address => device.ip_address,
                    :port_id => port["port_id"],
                    :local_port_id => port["local_port_id"],
                    :public_port_id => port["public_port_id"],
                    :bind_public_ip => port["bind_public_ip"]}
            rules.push(rule)
          end
        }
      }

      format_response(rules)
    end
   
    # Will replace any existing rule for the same device+port_id
    post '/gateways/:id/rules' do |id|
      rights = get_rights
      halt 401, format_response(error_authentication_failed) unless rights.includes?(id)

      rules = json_data[:rules]
      device_id = json_data[:device_id]
      rules_added = {:device_id => device_id, :gateway_id => id, :rules => []}

      # Get device
      device = Device.for_update.first(id: device_id, gateway_id: id)

      # Add each rule in turn
      rules.each { |rule|
        
        port_id = rule[:port_id]
        bind_public_ip = rule[:bind_public_ip]
      
        # Get port for device
        port = device.ports.detect {|_port| _port["port_id"].to_s == port_id.to_s }
        if port then

          # Check if already forwarded. If so at present only public binding
          # flag is updated
          if port["local_port_id"] then
            new_port = port.merge({"bind_public_ip" => bind_public_ip}) 
          else
            # FIXME: At present we create a public port whether or not it is to be bound.
            # This might be desirable. Not sure.
            new_port = port.merge({"local_port_id" => Ports.get_local_port(id),
                                "public_port_id" => Ports.get_public_port(id),
                                "bind_public_ip" => bind_public_ip})
          end

          device.ports.delete_if {|_port| _port["port_id"].to_s == port_id.to_s }
          device.ports.push(new_port)
      
          # If publicly bound Save a copy of the rule in KV Store, so the VPN Service can pick it up
          if new_port["bind_public_ip"] then
            PublicRules.add(id, new_port["public_port_id"], new_port["local_port_id"])      
          end

          rules_added[:rules].push(new_port)
     
        else
          # In this case just return an error
          rules_added[:rules].push({:error=>"Port doesn't exist."})
        end 
      }
      
      device.save
      
      format_response(rules_added)
    end

    delete '/gateways/:id/rules' do |id|
      rights = get_rights 
      halt 401, format_response(error_authentication_failed) unless rights.includes?(id)

      device_id = params[:device_id]
      port_id = params[:port_id]

      # Get ports for device
      device = Device.for_update.first(id: device_id, gateway_id: id)
      port = device.ports.detect {|_port| _port["port_id"].to_s == port_id.to_s }
      halt 404 unless port

      # Remove relevant keys. There should not be any chance of
      # a real collision of ports since the Gateway cannot
      # implement new rules without de-activating the old rules
      port.delete("bind_public_ip")
      local_port_id = port.delete("local_port_id")
      public_port_id = port.delete("public_port_id")
      Ports.release_local_port(local_port_id, id)
      Ports.release_public_port(public_port_id, id)

      # Create new ports hash
      ports = device.ports
      ports = ports.delete_if {|_port| _port["port_id"].to_s == port_id.to_s }
      ports.push(port)
      # update ports, calling override_ports=true (without it, ports cannot be removed)
      device.update({:ports => ports}, true)
      device.save

      # Remove any existing reference to rule in REDIS so VPN Service can remove it
      PublicRules.remove(id, public_port_id, local_port_id)
      
      status 204
    end
    
  end
  
end
