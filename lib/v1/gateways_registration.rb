module EvercamGateway

  class V1 < Sinatra::Base
    
    post '/gateways' do

      mac_address = json_data[:mac_address]
      m2m_secret = json_data[:m2m_secret]
      
      halt 422, format_response(error_operation_failed) unless mac_address
      
      # Check that data contains valid MAC Address
      halt 422, format_response(error_operation_failed) unless Validation.valid_mac_address?(mac_address)

      # If Gateway is already approved or registered then this endpoint is no longer relevant
      # We won't return a confirmation of that specifically, as there's no current
      # need to unnecessarily expose that information
      if Gateway.where(mac_address: mac_address).first then
        halt 404, format_response(error_operation_failed)
      end

      # Check if there is a 'secret' for this Gateway. If not then the Gateway
      # cannot register
      secret = Secret.where[mac_address: mac_address]
      halt 404, format_response(error_operation_failed) unless secret
      
      # If Gateway is pending then update it. If non-existent then add it
      pending_gateway = PendingGateway.where[mac_address: mac_address]
      if pending_gateway then
        # m2m secret must be provided
        halt 401, format_response(error_authentication_failed) unless m2m_secret && Secret.match?(mac_address, m2m_secret, true)
        
        pending_gateway.update({:originating_ip_address => request.ip})
        pending_gateway.save
        status 200
      else
        pending_gateway = PendingGateway.safe_new(json_data, request.ip)
        pending_gateway.save
        halt 400, format_response(error_operation_failed) unless pending_gateway
        pending_gateway = pending_gateway.values
        # Generate a secret which allows gateway to identify itself during registration
        pending_gateway[:m2m_secret] = secret.generate_m2m_secret
        status 201
      end
      
      # Send temporary gateway record
      format_response pending_gateway
    end

    # Only for pending gateways. Regex matches on valid mac_address
    get %r{\A\/gateways\/((?:[0-9A-Fa-f]{2}[:-]){5}(?:[0-9A-Fa-f]{2}))\Z} do |mac_address|
      secret = params[:secret]

      # Make sure the Gateway is actually pending
      pending_gateway = PendingGateway.where(mac_address: mac_address).first
      halt 404, format_response(error_operation_failed) unless pending_gateway

      # Check secret against MAC Address/Pre-generated secret
      halt 401, format_response(error_authentication_failed) unless Secret.match?(mac_address, secret)

      # Return pending Gateway information
      format_response(pending_gateway)
    end
    
    # patch /gateways/mac_address
    # This is where a gateway gets approved. Route is matched on
    # a valid MAC Address only. I use this rather poorly readable match
    # here because it is the simplest way to avoid conflict with the usual
    # /gateways/:id patch route which will be used for normal updates    
    patch %r{\A\/gateways\/((?:[0-9A-Fa-f]{2}[:-]){5}(?:[0-9A-Fa-f]{2}))\Z} do |mac_address|
      secret = json_data[:secret]
      gateway_name = json_data[:name]
      evercam_token = json_data[:evercam_token]
      
      # Make sure the Gateway is actually pending
      pending_gateway = PendingGateway.where(mac_address: mac_address).first
      halt 404, format_response(error_operation_failed) unless pending_gateway

      # Authenticate with Evercam to obtain Evercam User ID
      evercam = Evercam.authenticate(evercam_token)
      halt 401, format_response(error_authentication_failure) unless evercam

      # Check secret against MAC Address/Pre-generated secret
      halt 401, format_response(error_authentication_failed) unless Secret.match?(mac_address, secret)
      
      # Create Approved Gateway. Pass pending gateway so we'll always have
      # access to any data we add to that later.
      gateway = Gateway.new(evercam.id, gateway_name, pending_gateway)
      gateway.save
     
      # Remove pending Gateway
      pending_gateway.delete

      # Return new Gateway
      status 201
      headers \
        "Location" => "/gateways/#{gateway.id}"
      format_response(gateway)
    end

    get '/gateways/:mac_address/token' do |mac_address|
      m2m_secret = params[:m2m_secret]

      # Check that request contains valid MAC Address
      halt 422, format_response(error_operation_failed) unless Validation.valid_mac_address?(mac_address)

      # Match m2m secret
      halt 401, format_response(error_authentication_failed) unless Secret.match?(mac_address, m2m_secret, true)
      
      # If Gateway has been approved, but not yet registered then issue token
      if Gateway::approved?(mac_address) then
        gateway = Gateway.where[mac_address: mac_address]
        token = Token.new(gateway_id: gateway.id)
        token.save
        halt 201, format_response(token)
      elsif Gateway::registered?(mac_address) 
        halt 401, format_response(error_operation_failed)
      else
        halt 401, format_response(error_gateway_not_approved)
      end
      
    end
    
    # Allows gateway to obtain its own configuration. Sets status to registered.
    get '/gateways/:id/configuration' do |id|
      rights = get_rights
      halt 401, format_response(error_authentication_failed) unless rights.includes?(id)

      gateway = Gateway[id]
      
      # Contact VPN Service and get response
      response = JSON.parse(VPNService.get_gateway_configuration_data(id), symbolize_names: true)

      # Set the Gateway as registered
      gateway.update(registration_status: "registered", vpn_username: response[:vpn_username],
                      vpn_hub: response[:vpn_hub], vpn_port: response[:vpn_port], vpn_hostname: response[:vpn_hostname],vpn_local_hostname: response[:vpn_local_hostname], vpn_mac_address: response[:vpn_mac_address], vpn_ip_address: response[:vpn_ip_address])
      gateway.save
      
      configuration = gateway.values.only(:user_id, :vpn_username, :vpn_hub, :vpn_port, 
                                        :vpn_hostname, :vpn_local_hostname, :vpn_mac_address)
      configuration[:vpn_private_key] = response[:vpn_private_key]
      configuration[:vpn_public_key] = response[:vpn_certificate]
      configuration[:gateway_id] = gateway.id # !Important - return primary key as gateway_id
      
      format_response(configuration)
    end
   
  end
    
end
