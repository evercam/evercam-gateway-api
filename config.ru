require File.expand_path('../lib/v1.rb', __FILE__)

use Rack::ShowExceptions
use Rack::Cors do
  allow do
    origins '*'
    resource '/v1/*', :headers => :any, :methods => [:get, :post, :put, :delete, :patch, :options]
  end
end

map '/v1' do
  run EvercamGateway::V1
end
