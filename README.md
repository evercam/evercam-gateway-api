# evercam-gateway-api
Rest API layer for Evercam Gateway Project

| Name   | Evercam Gateway API   |
| --- | --- |
| Owner   | [@scandox](https://github.com/scandox)   |
| Dependencies Status   | [![Dependency Status](https://gemnasium.com/evercam/evercam-gateway-api.svg)](https://gemnasium.com/evercam/evercam-gateway-api)  |
| Version  | 0.9  |
| Evercam API Version  | 1.0  |
| Licence | [AGPL](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-%28agpl-3.0%29) |
